﻿var Circles = (function() {

  'use strict';

  var colors = ['rgb(88,150,255)', 'rgb(79,135,229)', 'rgb(66,113,191)', 'rgb(44,75,127)', 'rgb(22,38,64)'];

  var draw = function(canvas) {
    var context;
    var drops = 75;

    if (canvas.getContext) {
      context = canvas.getContext('2d');

      for (var i = 0; i < 3; i++) {
        for (var j = 0; j < drops; j++) {
          var x = offset(canvas.width / 2);
          var y = offset(canvas.height / 1.25);
          var r = Math.pow((1 + Math.sqrt(5)) / 4, Math.sqrt(Math.abs(x) + Math.abs(y))) * canvas.height;

          context.beginPath();
          context.fillStyle = colors[i+1];
          context.arc((canvas.width / 2) + x, y, r, 0, Math.PI * 2, true);
          context.fill();
        }

        drops -= 15;
      }
    }
  };

  var offset = function(range) {
    return Math.floor(Math.random() * range) * (Math.round(Math.random()) * 2 - 1);
  };

  return {
    draw: draw
  };

})();
